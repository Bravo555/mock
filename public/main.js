function mock(text) {
    let newText = '';
    newText += text[0].toLowerCase();
    for(i = 1; i < text.length; i++) {
        if(i % 2 !== 0) {
            newText += text[i].toUpperCase();
        } else {
            newText += text[i];
        }
    }
    return newText;
}

function update(src, dst) {
    let text = src.value;
    dst.value = mock(text);
}

let src = document.getElementById('src');
let dst = document.getElementById('dst');
src.addEventListener('paste', () => update(src, dst));
src.addEventListener('input', () => update(src, dst));